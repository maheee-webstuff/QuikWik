
export class Constants {
  static readonly DEFAULT_CONTENT: string = '';
  static readonly DEFAULT_X: number = 100;
  static readonly DEFAULT_Y: number = 100;
  static readonly DEFAULT_WIDTH: number = 300;
  static readonly DEFAULT_HEIGHT: number = 300;

  static readonly PAGE_MIN_WIDTH: number = 250;
  static readonly PAGE_MIN_HEIGHT: number = 100;

  static readonly DESKTOP_MATCH = "(min-width: 600px) and (min-height: 600px)";
}