import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs';

import { Constants } from '../constants';


@Injectable()
export class MobileService {

  private _isMobile: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  isMobile$: Observable<boolean>;

  constructor() {
    let match = window.matchMedia(Constants.DESKTOP_MATCH);
    if (match.matches) {
      this.manuallySetDesktop();
    } else {
      this.manuallySetMobile();
    }
    this.isMobile$ = this._isMobile.asObservable();
  }

  manuallySetMobile() {
    this._isMobile.next(true);
  }

  manuallySetDesktop() {
    this._isMobile.next(false);
  }

}
