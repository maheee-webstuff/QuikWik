import { Component, ElementRef, Input, OnChanges } from '@angular/core';
import * as marked from 'marked';


declare var Prism: any;


const linkRegex = new RegExp(/^!?\[(label)\]\(href(?:\s+(title))?\s*\)/.source
  .replace('label', /(?:\[[^\[\]]*\]|\\[\[\]]?|`[^`]*`|[^\[\]\\])*?/.source)
  .replace('href', /\s*(<(?:\\[<>]?|[^\s<>\\])*>|(?:\\[()]?|\([^\x00-\x1f\\]*\)|[^\x00-\x1f()\\])*?)/.source)
  .replace('title', /"(?:\\"?|[^"\\])*"|\'(?:\\\'?|[^\'\\])*\'|\((?:\\\)?|[^)\\])*\)/.source));


@Component({
  selector: 'markdown,[Markdown]',
  template: '<ng-content></ng-content>',
  styles: [
    `.token.operator, .token.entity, .token.url, .language-css .token.string, .style .token.string {
      background: none;
    }
    :host ::ng-deep * {
      cursor: initial;
      -webkit-user-select: text;
    }
    `
  ]
})
export class MarkdownComponent implements OnChanges {

  private renderer;

  @Input()
  private rawText: string;

  constructor(private el: ElementRef) {
    // patching marked to allow links with spaces in href
    (<any>marked).InlineLexer.rules.link = linkRegex;
    (<any>marked).InlineLexer.rules.gfm.link = linkRegex;
    (<any>marked).InlineLexer.rules.normal.link = linkRegex;

    marked.setOptions({
      highlight: (code: string, lang: string, callback: Function) => {
        if (lang && Prism.languages[lang]) {
          code = Prism.highlight(code, Prism.languages[lang]);
        }
        if (callback) {
          callback(null, code);
        }
        return code;
      }
    });

    this.renderer = new marked.Renderer();
    this.renderer.link = (href: string, title: string, text: string) => {
      if (text == 'qw') {
        return `<a class="wikilink" href="javascript:" onClick="openPage('${href}')"><span class="icon icon-login"></span>${href}</a>`;
      } else {
        if (title) {
          return `<a href="${href}" title="${title}" target="_blank"><span class="icon icon-logout"></span>${text}</a>`;
        } else {
          return `<a href="${href}" target="_blank"><span class="icon icon-logout"></span>${text}</a>`;
        }
      }
    };
  }

  ngOnChanges() {
    let md = this.prepare(this.rawText);
    this.el.nativeElement.innerHTML = marked(md, {
      renderer: this.renderer,
    });
  }

  private prepare(raw: any) {
    if (!raw) {
      return '';
    }
    return raw;
  }

}
