import { Component, Input, Output, EventEmitter, OnInit, ViewChild, ElementRef } from '@angular/core';


declare var Prism: any;


@Component({
  selector: 'app-source',
  templateUrl: './source.component.html',
  styleUrls: ['./source.component.scss']
})
export class SourceComponent implements OnInit {

  @ViewChild('el')
  el: ElementRef;

  @Input()
  source: string;

  @Output()
  sourceChange: EventEmitter<string> = new EventEmitter<string>();

  ngOnInit() {
    const range = document.createRange();
    range.setStart(this.el.nativeElement, 0);
    const selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(range);
  }

  formatSource(source) {
    return Prism.highlight(source, Prism.languages.markdown);
  }

  onChange(text) {
    this.source = text;
    this.sourceChange.emit(text);
  }

}
