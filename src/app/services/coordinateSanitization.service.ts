import { Injectable } from "@angular/core";

import { Constants } from "../constants";
import { WikiPage } from "../model/wikipage";


@Injectable()
export class CoordinateSanitizationService {

  getSaneCoordinates(page: WikiPage) {
    let iw = window.innerWidth;
    let ih = window.innerHeight;
    let ww = iw - Constants.DEFAULT_WIDTH;
    let wh = ih - Constants.DEFAULT_HEIGHT;

    let result = {
      x: page.x,
      y: page.y,
      width: page.width,
      height: page.height
    };

    if (result.x < 0) {
      result.x = 0;
    }
    if (result.y < 0) {
      result.y = 0;
    }
    if (result.x > ww) {
      result.x = ww;
    }
    if (result.y > wh) {
      result.y = wh;
    }
    if (result.x + result.width > iw) {
      result.width = iw - result.x;
    }
    if (result.y + result.height > ih) {
      result.height = ih - result.y;
    }

    return result;
  }

}