# QuikWik [![pipeline status](https://gitlab.com/maheee-webstuff/QuikWik/badges/master/pipeline.svg)](https://gitlab.com/maheee-webstuff/QuikWik/commits/master)


## Description

QuikWik is a small and simple Wiki which uses Markdown Syntax. All the data is stored in the local storage in the browser or remote via [RemoteStorage](https://remotestorage.io/).

![Screenshot](./res/screenshot.png "Screenshot")

### Syntax

As it uses [marked](https://github.com/chjj/marked) for rendering, it supports everything [marked](https://github.com/chjj/marked) supports.

There is one addition to support linking between pages:

```
[qw](Name of linked page)
```

## Live Version

Latest build is available here:
- https://maheee-webstuff.gitlab.io/QuikWik/

Latest release is available on 5apps:
- https://quik-wik.5apps.com/


## Used Libraries

**Frontend:**
- http://photonkit.com/

**Markdown and Code formatting:**
- https://github.com/chjj/marked
- http://prismjs.com/

**Storage:**
- https://remotestorage.io/


## Development

This project was generated with [angular-cli](https://github.com/angular/angular-cli).

### Setup

Run `npm install` to install all dependencies.

### Development server

Run `npm start` for a dev server. Navigate to [http://localhost:4200/](http://localhost:4200/). The app will automatically reload if you change any of the source files.

### Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.
