import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { PageService } from './services/page.service';
import { StorageService } from './services/storage/storage.service';
import { MobileService } from './services/mobile.service';
import { WikiPage } from './model/wikipage';

import { environment } from '../environments/environment';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  isMobile$: Observable<boolean>;
  pages$: Observable<string[]>;

  data: string;
  showDataField: boolean = false;

  pageTitleList: string[] = [];

  version = environment.version;

  constructor(
    private storageService: StorageService,
    private pageService: PageService,
    private mobileService: MobileService) {

    this.isMobile$ = mobileService.isMobile$;
    this.pages$ = pageService.getPages();
  }

  ngOnInit() {
    this.pageService.getPages();
    this.storageService.displayWidget();
    this.start();

    this.storageService.syncDone$.subscribe(() => {
      this.storageService.list().then(res => {
        this.pageTitleList = Object.keys(res).sort().map(pageId => this.pageService.id2title(pageId));
      });
    });
  }

  openPage(title) {
    this.pageService.openPage(title);
  }

  start() {
    this.pageService.openPage('Start');
  }

  manuallySetMobile() {
    this.mobileService.manuallySetMobile();
  }

  manuallySetDesktop() {
    this.mobileService.manuallySetDesktop();
  }

}
