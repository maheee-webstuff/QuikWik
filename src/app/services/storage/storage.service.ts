import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { filter } from 'rxjs/operators';

import { Module } from './module';
import { WikiPage } from '../../model/wikipage';


declare var Widget, RemoteStorage;


@Injectable()
export class StorageService {

  private remoteStorageWidget;
  private remoteStorage;
  private changeSubject: Subject<any> = new Subject<any>();
  private syncDoneSubject: Subject<any> = new Subject<any>();

  public syncDone$ = this.syncDoneSubject.asObservable();

  constructor() {
    this.remoteStorage = new RemoteStorage();
    Module.defineModule(this.remoteStorage);
    this.remoteStorage.access.claim('wiki', 'rw');
    this.remoteStorageWidget = new Widget(this.remoteStorage);

    this.remoteStorage.on('sync-done', () => {
      this.syncDoneSubject.next();
    });

    this.remoteStorage
      .wiki
      .onChange(event => {
        this.changeSubject.next(event);
      });
  }

  getChangeObservable(id: string) {
    return this.changeSubject.pipe(
      filter(event => event.newValue && event.relativePath === id)
    );
  }

  displayWidget(): void {
    this.remoteStorageWidget.attach();
  }

  list(): Promise<any> {
    return this.remoteStorage
      .wiki
      .list();
  }

  load(id: string): Promise<WikiPage> {
    return this.remoteStorage
      .wiki
      .get(id);
  }

  save(id: string, wikiPage: WikiPage): Promise<WikiPage> {
    return this.remoteStorage
      .wiki
      .set(id, wikiPage);
  }

  remove(id: string): Promise<any> {
    return this.remoteStorage
      .wiki
      .remove(id);
  }

}
