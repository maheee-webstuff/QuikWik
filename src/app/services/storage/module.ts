import { WikiPage } from '../../model/wikipage';

import RemoteStorage from 'remotestoragejs';


export class Module {

  private static declareType(privateClient) {
    privateClient.declareType('page', {
      type: 'object',
      properties: {
        title: { type: 'string', required: true },
        content: { type: 'string', required: true, default: '' },

        x: { type: 'integer', required: false },
        y: { type: 'integer', required: false },
        width: { type: 'integer', required: false },
        height: { type: 'integer', required: false },

        lastEdited: { type: 'integer', required: true },
      }
    });
  }

  private static defineModuleExports(privateClient) {
    return {
      set: (id: string, page: WikiPage): Promise<WikiPage> => {
        page.lastEdited = (+ Date.now());
        return privateClient.storeObject('page', id, page);
      },

      get: (id: string): Promise<WikiPage> => {
        return privateClient.getObject(id.toString());
      },

      remove: (id: string): Promise<any> => {
        return privateClient.remove(id.toString());
      },

      list: (): Promise<any> => {
        return privateClient.getListing('/')
      },

      onChange: (handler) => {
        privateClient.addEventListener('change', event => {
          handler(event);
        });
      }
    }
  }

  static defineModule(remoteStorage: RemoteStorage) {
    const wikiModule = {
      name: 'wiki', builder: (privateClient, publicClient) => {
        this.declareType(privateClient);
        return {
          exports: this.defineModuleExports(privateClient)
        }
      }
    };
    remoteStorage.addModule(wikiModule);
  }

}
