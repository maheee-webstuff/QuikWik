import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { StorageService } from './storage/storage.service';
import { WikiPage } from '../model/wikipage';
import { Constants } from '../constants';


@Injectable()
export class PageService {
  private pages: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);

  private lastZIndex: number = 1;

  constructor(private storageService: StorageService) {
    window['openPage'] = (id) => {
      this.openPageById(id);
    }
  }

  getChangeObservable(title: string) {
    const id = this.title2id(title);
    return this.storageService.getChangeObservable(id);
  }

  getPages() {
    return this.pages.asObservable();
  }

  openPageById(id: string) {
    this.openPage(this.id2title(id));
  }

  openPage(title: string) {
    title = this.sanitizeTitle(title);
    this.closePage(title);

    let pagesList = this.pages.getValue();
    pagesList.push(title);
    this.pages.next(pagesList);
  }

  closePage(title: string) {
    title = this.sanitizeTitle(title)

    let pagesList = this.pages.getValue();
    for (let i = 0; i < pagesList.length; ++i) {
      if (pagesList[i] == title) {
        pagesList.splice(i, 1);
        break;
      }
    }

    this.pages.next(pagesList);
  }

  closeAllPages() {
    this.pages.next([]);
  }

  loadPage(title: string): Promise<WikiPage> {
    let id = this.title2id(title);

    return new Promise<WikiPage>((resolve, reject) => {
      this.storageService.load(id).then(page => {
        if (page) {
          resolve(page);
        } else {
          resolve(new WikiPage(
            this.sanitizeTitle(title),
            Constants.DEFAULT_CONTENT,
            Constants.DEFAULT_X,
            Constants.DEFAULT_Y,
            Constants.DEFAULT_WIDTH,
            Constants.DEFAULT_HEIGHT));
        }
      }).catch(error => {
        reject(error);
      });
    });
  }

  savePage(page: WikiPage): Promise<WikiPage> {
    let id = this.title2id(page.title);

    return this.storageService.save(id, page);
  }

  removePage(title: string): Promise<any> {
    let id = this.title2id(title);

    return this.storageService.remove(id);
  }

  getNextZIndex() {
    return ++this.lastZIndex;
  }

  sanitizeTitle(title: string) {
    return title.trim().replace('_', ' ');
  }

  sanitizeId(id: string) {
    return id.trim().replace(' ', '_');
  }

  title2id(title: string) {
    return 'page__' + title.trim().replace(' ', '_');
  }

  id2title(id: string) {
    return id.trim().replace(/^page__/, '').replace('_', ' ');
  }

}
