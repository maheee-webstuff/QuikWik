import { Constants } from "../../constants";

export abstract class DraggableResizable {

  abstract getNextZIndex(): number;

  x: number = 100;
  y: number = 100;
  width: number = 300;
  height: number = 300;

  maximized: boolean = false;

  zindex: number = 10;

  hasMoved: boolean = false;

  private lastPosX: number;
  private lastPosY: number;
  private dragging: boolean = false;
  private resizing: boolean = false;

  onDraggerMouseDown(event) {
    this.startAction(event);
    this.dragging = true;
  }

  onResizerMouseDown(event) {
    this.startAction(event);
    this.resizing = true;
  }

  private startAction(event) {
    this.lastPosX = event.clientX;
    this.lastPosY = event.clientY;
    this.zindex = this.getNextZIndex();
  }

  onMouseUp(event) {
    this.dragging = false;
    this.resizing = false;
  }

  onMouseLeave(event) {
    this.onMouseUp(event);
  }

  onMouseMove(event) {
    if (this.maximized) {
      return;
    }
    if (this.dragging) {
      this.drag(event);
      this.clearSelection();
    }
    if (this.resizing) {
      this.resize(event);
      this.clearSelection();
    }
    this.lastPosX = event.clientX;
    this.lastPosY = event.clientY;
  }

  private drag(event) {
    this.x += event.clientX - this.lastPosX;
    this.y += event.clientY - this.lastPosY;

    this.hasMoved = true;
  }

  private resize(event) {
    this.width += event.clientX - this.lastPosX;
    this.height += event.clientY - this.lastPosY;
    if (this.width < Constants.PAGE_MIN_WIDTH) {
      this.width = Constants.PAGE_MIN_WIDTH;
    }
    if (this.height < Constants.PAGE_MIN_HEIGHT) {
      this.height = Constants.PAGE_MIN_HEIGHT;
    }

    this.hasMoved = true;
  }

  toggleMaximize() {
    this.maximized = !this.maximized;
  }

  protected maximize() {
    this.maximized = true;
  }

  protected unmaximize() {
    this.maximized = false;
  }

  private clearSelection() {
    let selection = window.getSelection ? window.getSelection() : document['selection'];
    if (selection) {
      if (selection.removeAllRanges) {
        selection.removeAllRanges();
      } else if (selection.empty) {
        selection.empty();
      }
    }
  }

}