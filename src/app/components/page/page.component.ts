import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { PageService } from '../../services/page.service';
import { MobileService } from '../../services/mobile.service';
import { WikiPage } from '../../model/wikipage';
import { DraggableResizable } from './draggableresizable';
import { CoordinateSanitizationService } from '../../services/coordinateSanitization.service';


@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent extends DraggableResizable implements OnInit, OnDestroy {

  @Input()
  title: string;

  page: WikiPage;

  isViewActive: boolean = true;
  isSourceActive: boolean = false;

  floatingSource: string = '';

  isMobile: boolean;

  isLoading: boolean = true;
  isFailed: boolean = false;

  private isMobileSubscription: Subscription;
  private changeSubscription: Subscription;

  constructor(
      private mobileService: MobileService,
      private pageService: PageService,
      private coordinateSanitizationService: CoordinateSanitizationService) {
    super();
  }

  getNextZIndex() {
    return this.pageService.getNextZIndex();
  }

  ngOnInit() {
    this.pageService.loadPage(this.title).then(page => {
      this.setPage(page);
      this.isLoading = false;
    }).catch(err => {
      console.error(err);
      this.isLoading = false;
      this.isFailed = true;
    });
    this.zindex = this.pageService.getNextZIndex();

    this.isMobileSubscription = this.mobileService.isMobile$.subscribe(isMobile => {
      this.isMobile = isMobile;
      if (isMobile) {
        this.maximize();
      }
    });

    this.changeSubscription = this.pageService.getChangeObservable(this.title).subscribe(event => {
      this.setPage(event.newValue);
    });
  }

  ngOnDestroy() {
    this.isMobileSubscription.unsubscribe();
    this.changeSubscription.unsubscribe();
  }

  toggleSource() {
    if (!this.page) {
      return;
    }
    if (this.isViewActive) {
      this.switchToSource();
    } else {
      this.onRevert();
    }
  }

  onFloatingSourceChange(source) {
    this.floatingSource = source;
  }

  onRevert() {
    this.floatingSource = this.page.content;

    this.switchToView();
  }

  onSave() {
    this.savePage();
    this.switchToView();
  }

  onDelete() {
    this.pageService.removePage(this.page.title);
    this.pageService.closePage(this.page.title);
  }

  onClose() {
    if (!this.maximized && this.hasMoved) {
      if (this.page.x !== this.x ||
          this.page.y !== this.y ||
          this.page.width !== this.width ||
          this.page.height !== this.height) {
        this.savePage(true);
      }
    }

    this.pageService.closePage(this.page.title);
  }

  private setPage(page: WikiPage) {
    this.page = page;
    let saneCoordinates = this.coordinateSanitizationService.getSaneCoordinates(page);
    this.x = saneCoordinates.x;
    this.y = saneCoordinates.y;
    this.width = saneCoordinates.width;
    this.height = saneCoordinates.height;
  }

  private switchToView() {
    this.isViewActive = true;
    this.isSourceActive = false;
  }

  private switchToSource() {
    this.floatingSource = this.page.content;
    this.isViewActive = false;
    this.isSourceActive = true;
  }

  private savePage(positionOnly: boolean = false) {
    if (!positionOnly) {
      this.page.content = this.floatingSource;
    }
    this.page.x = this.x;
    this.page.y = this.y;
    this.page.width = this.width;
    this.page.height = this.height;

    this.pageService.savePage(this.page).catch(err => {
      // FAIL
      console.error(err);
    });
  }

}
