import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { PageComponent } from './components/page/page.component';
import { MarkdownComponent } from './components/markdown/markdown.component';
import { SourceComponent } from './components/source/source.component';

import { StorageService } from './services/storage/storage.service';
import { PageService } from './services/page.service';
import { MobileService } from './services/mobile.service';
import { CoordinateSanitizationService } from './services/coordinateSanitization.service';


@NgModule({
  declarations: [
    AppComponent,
    PageComponent,
    MarkdownComponent,
    SourceComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    StorageService,
    PageService,
    MobileService,
    CoordinateSanitizationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
